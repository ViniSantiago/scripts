import http from "k6/http";
import { check, sleep } from "k6";

export let options = {
  rps: 1000,
  vus: 98,
  duration: '30s'
};

export default function() {
  let res = http.get("https://httpbin.org/");
  check(res, {
    "status was 200": (r) => r.status == 200
  });
  // sleep(1);
}